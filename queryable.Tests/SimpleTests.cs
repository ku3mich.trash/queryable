﻿using System;
using System.Linq;
using Xunit;

namespace queryable.Tests
{
    public class SimpleTests
    {
        private readonly IQueryable<Account> Query;

        public SimpleTests()
        {
            Query = Factory.GetQuery<Account>();
        }

        [Fact]
        public void Single()
        {
            var single = Query.Single();
            Assert.NotNull(single);
        }

        [Fact]
        public void Single_With_Expression()
        {
            // expression comes to Execute
            var single = Query.Single(s => s.Id == 0);
            Assert.NotNull(single);
        }

        [Fact]
        public void Single_With_Where_Expression()
        {
            // invoking CreateQuery
            var single = Query.Where(s => s.Id == 0).Single();
            Assert.NotNull(single);
        }
    }
}
