﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace queryable
{
    public class MyQueryProvider : IQueryProvider
    {
        public IQueryable CreateQuery(Expression expression) => CreateQuery<object>(expression);
        public IQueryable<TElement> CreateQuery<TElement>(Expression expression) => new MyQueryable<TElement>();

        public object Execute(Expression expression) => Execute<object>(expression);

        public TResult Execute<TResult>(Expression expression)
        {
            return Activator.CreateInstance<TResult>();
        }
    }
}
