﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace queryable
{
    public class MyQueryable<T> : IQueryable<T>
    {
        public Type ElementType { get; } = typeof(T);
        public Expression Expression { get; } = Expression.Constant(Array.Empty<T>().AsQueryable());
        public IQueryProvider Provider { get; } = new MyQueryProvider();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<T> GetEnumerator()
        {
            return null;
        }
    }
}
