﻿using System.Linq;

namespace queryable
{
    public static class Factory
    {
        public static IQueryable<T> GetQuery<T>() => new MyQueryable<T>();
    }
}
